/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    USERNAME: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    USERPASS: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    REGISTERTIME: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    tableName: 'users'
  });
};
