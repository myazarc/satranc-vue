/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('votes', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    USERID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'users',
        key: 'ID'
      }
    },
    VOTE: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    VOTEDESCRIPTION: {
      type: DataTypes.STRING(200),
      allowNull: true
    }
  }, {
    tableName: 'votes'
  });
};
