const Sequelize = require('sequelize');

// Veritabanı bilgileri
const dbUser = 'root';
const dbPass = '12345';
const dbName = 'satranc';
const dbHost = 'localhost';
// Sequelizenin operatörlerini kısaltıyoruz
const Op = Sequelize.Op;
const operatorsAliases = {
  $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col,
};
// sequelizeye bağlantımızı entegre ediyoruz ve pool bağlantı kuruyoruz.
const sequelize = new Sequelize(dbName, dbUser, dbPass, {
  host: dbHost,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  operatorsAliases,
  define: {
    timestamps: false,
  },
});

// modellerimizi import edip arraya alıyoruz
/* eslint-disable */
let modules = [
  require('./models/users.js'),
  require('./models/votes.js'),
];
let models={
  sequelize
};
// modellerimiz models objesine atıyoruz
modules.forEach((module) => {
  const model = module(sequelize, Sequelize);
  models[model.name] = model;
});

module.exports=models;