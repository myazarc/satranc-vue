const express = require('express');
const uses = require('./uses');

const db = require('./database');

// sunucumuzu oluşturalım
const app = express();
// oturum yönetimi entegre ediyoruz
uses(app);

// yayınlayacağımız port
const port = 8081;

// sonra da yönlendirmeleri yapalım
app.get('/', (req, res) => {
  const reqTime = parseInt(Date.now() / 1000);
  db.users.create({
    USERNAME: 'test',
    USERPASS: 'test',
    REGISTERTIME: reqTime,
  }).then(() => {
    db.users.findAll({ raw: true }).then((rows) => {
      console.log(rows);
    });
  }).catch((err) => {
    console.log('Sorgu Hata');
    console.log(err);
  });
  
  // kullanıcıya cevap olarak bunu döndürüyoruz
  res.send('Merhaba Satranç\'a Hoşgeldin!');
});

// uygulamayı yayınlıyoruz
app.listen(port, () => {
  console.log(`Uygulama ${port} portundan sunuluyor`);
});
