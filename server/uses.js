const session = require('express-session');
const redis = require('redis'); // bağlantı açmamız için gerekli bir kütüphane
const RedisStore = require('connect-redis')(session); // connect-redis'e express-session'u bağladık

module.exports = (app) => {
  // yeni bir redis istemcisi oluşturduk
  const client = redis.createClient();
  // oturum modeli
  app.use(session({
    store: new RedisStore({ // yeni bir redis bağlantısı
      client,
      host: 'localhost',
      port: 6379,
      ttl: 260,
    }),
    secret: 'gizli anahtar',
    resave: false,
    saveUninitialized: true,
  }));
};
